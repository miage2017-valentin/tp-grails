[Documentation API Swagger](https://app.swaggerhub.com/apis/mbds_miage_2018-2019/tp_grails/1.0.0-oas3)

# Installation
Commencez tout d'abord par cloner le projet à 
l'aide d'un terminal ou d'un IDE:
```
git clone https://bitbucket.org/miage2017-valentin/tp-grails.git
```

####Configuration
Modifiez ensuite les variables "cdnFolder" et "cdnRootUrl" dans 
le fichier de configuration "grails-app/conf/application.yml" 
avec vos informations :
```
    uploadServer:
        cdnFolder: 'C:\logiciel\wamp64\www'
        cdnRootUrl: 'http://localhost'
```

Vous pouvez maintenant lancer le projet en utilisant 
votre IDE (application développée avec l'IDE IntelliJ).

#Utilisation de la partie administrateur
Par défaut, la partie administrateur sera disponible à l'adresse
"http://localhost:8080".

Pour se connecter, vous pouvez utiliser le compte administrateur 
suivant:

- Identifiant : gali
- Mot de passe: gali

Les deux bonus ont été implémentés.

- Pour la possibilité d'uploader les fichiers en Ajax, nous utilisons un formulaire avec jQuery pour envoyer une requête AJAX. Pour le drag'n drop, nous avons juste utilisé les évènements Javascript "drop"/"dragover"/dragenter"/"dragleave"/...
- Pour supprimer tous les messages lus, nous utilisons un "CRON Job" qui est défini dans la classe "PurgeMessageReadJob".

#Utilisation de l'API
[Documentation API Swagger](https://app.swaggerhub.com/apis/mbds_miage_2018-2019/tp_grails/1.0.0-oas3)

Une collection et un environnement POSTMAN sont disponibles dans le dossier
"/docs/postman".

Vous pouvez alors lancer la collection avec l'environnement.
La collection permet de récupérer un token d'authentification 
et de lancer toutes les requêtes de test.

La couche sécurité a été implémentée en utilisant le module "Spring Security REST". Sa configuration se trouve dans "application.groovy".