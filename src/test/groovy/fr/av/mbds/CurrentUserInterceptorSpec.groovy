package fr.av.mbds

import grails.testing.web.interceptor.InterceptorUnitTest
import spock.lang.Specification

class CurrentUserInterceptorSpec extends Specification implements InterceptorUnitTest<CurrentUserInterceptor> {

    def setup() {
    }

    def cleanup() {

    }

    void "Test currentUser interceptor matching"() {
        when:"A request matches the interceptor"
            withRequest(controller:"currentUser")

        then:"The interceptor does match"
            interceptor.doesMatch()
    }
}
