package fr.av.mbds.command

import grails.config.Config
import grails.core.support.GrailsConfigurationAware
import grails.validation.Validateable
import org.springframework.web.multipart.MultipartFile

class ProfileImageCommand implements Validateable, GrailsConfigurationAware {
    MultipartFile imageFile
    int maxFileSize

    @Override
    void setConfiguration(Config co) {
        maxFileSize = co.getRequiredProperty('grails.uploadServer.maxSizeUploadFile')
    }

    static constraints = {
        imageFile  validator: { val, obj ->
            if ( val == null ) {
                return 'validation.fileInvalid'
            }
            if ( val.empty ) {
                return 'validation.fileInvalid'
            }

            if (val.size >= (5 * 1024 * 1024)) {
                return 'validation.sizeUploadFileExceeded'
            }

            def value = ['jpeg', 'jpg', 'png'].any { extension ->
                val.originalFilename?.toLowerCase()?.endsWith(extension)
            }

            if (!value) {
                return 'validation.extensionInvalid'
            }
        }
    }
}