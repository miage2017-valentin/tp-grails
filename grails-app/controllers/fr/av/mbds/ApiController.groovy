package fr.av.mbds

import fr.av.mbds.api.ResponseService
import fr.av.mbds.exceptions.ApiException
import fr.av.mbds.impl.GameEntityService
import fr.av.mbds.impl.MessageEntityService
import fr.av.mbds.impl.UserEntityService
import grails.plugin.springsecurity.SpringSecurityService
import grails.validation.ValidationException

class ApiController {

    ResponseService responseService
    MessageService messageService
    GameService gameService
    MessageEntityService messageEntityService
    GameEntityService gameEntityService
    UserEntityService userEntityService
    SpringSecurityService springSecurityService

    def message(Long id) {
        User currentUser = springSecurityService.currentUser

        switch (request.getMethod()) {
            case 'GET':
                Message message = Message.findById(id)
                if (message != null) {
                    if (message.author == currentUser || message.target == currentUser) {
                        if (message.target == currentUser && !message.isRead) {
                            message.isRead = true
                            message.save()
                        }
                        render(responseService.createSuccessResponse(response, message))
                    } else {
                        render(responseService.createErrorResponse(response, 'Not authorized', 401))
                    }
                } else {
                    render(responseService.createErrorResponse(response, 'Message not found', 404))
                }
                break
            case 'POST':
                def json = request.JSON
                try {
                    Message newMessage = messageEntityService.createMessage(request.JSON)
                    render(responseService.createSuccessResponse(response, newMessage, 'Message created', 201))
                } catch (ApiException ex) {
                    render(responseService.createErrorResponse(response, ex.getMessage(), ex.getCodeHTTP()))
                }
                break
            case 'PUT':
                Message message = Message.findById(id)

                if (message == null) {
                    render(responseService.createErrorResponse(response, 'Message not found', 404))
                } else {
                    if (message.author != currentUser) {
                        render(responseService.createErrorResponse(response, 'Not authorized to update this message', 401))
                    } else {
                        def json = request.JSON
                        message.content = json.content
                        try {
                            messageService.save(message)
                            render(responseService.createSuccessResponse(response, message, 'Message updated', 200))
                        } catch (ValidationException e) {
                            render(responseService.createErrorResponse(response, e.getMessage(), 400))
                        }
                    }
                }
                break
            case 'DELETE':
                Message message = Message.findById(id)

                if (message == null) {
                    render(responseService.createErrorResponse(response, 'Message not found', 404))
                } else {
                    if (message.author != currentUser) {
                        render(responseService.createErrorResponse(response, 'Not authorized to delete this message', 401))
                    } else {
                        try {
                            messageService.delete(message.id)
                            render(responseService.createSuccessResponse(response, message, 'Message removed', 200))
                        } catch (ValidationException e) {
                            render(responseService.createErrorResponse(response, e.getMessage(), 400))
                        }
                    }
                }
                break
            default:
                render(responseService.createErrorResponse(response, 'Method not implemented', 405))
        }
    }

    def messages() {
        User currentUser = springSecurityService.currentUser

        switch (request.getMethod()) {
            case 'GET':
                def typeMessage = request.getParameter('typeMessage')
                Set<Message> messages
                switch (typeMessage) {
                    case 'received':
                        messages = currentUser.messageReceived
                        break
                    case 'sent':
                        messages = currentUser.messageSent
                        break
                    default:
                        messages = currentUser.messageSent + currentUser.messageReceived
                }
                render(responseService.createSuccessResponse(response, messages))
                break
            case 'POST':
                println(request.JSON)
                try {
                    List<Message> listMessages = []
                    request.JSON.each {
                        listMessages.add(messageEntityService.createMessage(it))
                    }
                    render(responseService.createSuccessResponse(response, listMessages, 'Messages created', 201))
                } catch (ApiException ex) {
                    render(responseService.createErrorResponse(response, ex.getMessage(), ex.getCodeHTTP()))
                }
                break
            default:
                render(responseService.createErrorResponse(response, 'Method not implemented', 405))
        }
    }

    def game(Long id) {
        User currentUser = springSecurityService.currentUser

        switch (request.getMethod()) {
            case 'GET':
                if (id == null) {
                    render(responseService.createErrorResponse(response, 'Id empty'))
                }
                Game game = Game.findById(id)
                if (game != null) {
                    if (game.winner == currentUser || game.looser == currentUser) {
                        render(responseService.createSuccessResponse(response, game))
                    } else {
                        render(responseService.createErrorResponse(response, 'Not authorized', 401))
                    }
                } else {
                    render(responseService.createErrorResponse(response, 'Game not found', 404))
                }
                break
            case 'POST':
                try {
                    Game newGame = gameEntityService.createGame(request.JSON)
                    render(responseService.createSuccessResponse(response, newGame, 'Game created', 201))
                } catch (ApiException ex) {
                    render(responseService.createErrorResponse(response, ex.getMessage(), ex.getCodeHTTP()))
                }
                break
            case 'PUT':
                Game game = Game.findById(id)
                if (game == null) {
                    render(responseService.createErrorResponse(response, 'Game not found', 404))
                } else {
                    if (game.winner != currentUser && game.looser != currentUser) {
                        render(responseService.createErrorResponse(response, 'Not authorized to update this game', 401))
                    } else {
                        game.properties = request.JSON
                        try {
                            gameService.save(game)
                            render(responseService.createSuccessResponse(response, game, 'Game updated', 200))
                        } catch (ValidationException e) {
                            render(responseService.createErrorResponse(response, e.getMessage(), 400))
                        }
                    }
                }

                break
            case 'DELETE':
                Game game = Game.findById(id)
                if (game == null) {
                    render(responseService.createErrorResponse(response, 'Game not found', 404))
                } else {
                    if (game.winner != currentUser && game.looser != currentUser) {
                        render(responseService.createErrorResponse(response, 'Not authorized to delete this game', 401))
                    } else {
                        try {
                            gameService.delete(game.id)
                            render(responseService.createSuccessResponse(response, game, 'Game removed', 200))
                        } catch (ValidationException e) {
                            render(responseService.createErrorResponse(response, e.getMessage(), 400))

                        }
                    }
                }
                break
            default:
                render(responseService.createErrorResponse(response, 'Method not implemented', 405))
        }

    }

    def games() {
        User currentUser = springSecurityService.currentUser

        switch (request.getMethod()) {
            case 'GET':
                def status = request.getParameter('status')
                Set<Game> games
                switch (status) {
                    case 'winner':
                        games = currentUser.matchWon
                        break
                    case 'looser':
                        games = currentUser.matchLost
                        break
                    default:
                        games = currentUser.matchLost + currentUser.matchWon
                }
                render(responseService.createSuccessResponse(response, games))
                break
            case 'POST':
                try {
                    List<Game> listGames = []
                    request.JSON.each {
                        listGames.add(gameEntityService.createGame(it))
                    }
                    render(responseService.createSuccessResponse(response, listGames, 'Games created', 201))
                } catch (ApiException ex) {
                    render(responseService.createErrorResponse(response, ex.getMessage(), ex.getCodeHTTP()))
                }
                break
            default:
                render(responseService.createErrorResponse(response, 'Method not implemented', 405))
        }
    }


    def user(Long id) {
        User currentUser = springSecurityService.currentUser

        switch (request.getMethod()) {
            case 'GET':
                User user = User.findById(id)
                if (user != null) {
                    render(responseService.createSuccessResponse(response, user, 'User found'))
                } else {
                    render(responseService.createErrorResponse(response, 'User not found', 404))
                }
                break
            case 'POST':
                Role roleUser = Role.findByAuthority('ROLE_USER')
                try {
                    User user = userEntityService.createUserWithDefaultRole(request.JSON, [roleUser.id.toString()])
                    render(responseService.createSuccessResponse(response, user, 'User created', 201))
                } catch (ApiException ex) {
                    render(responseService.createErrorResponse(response, ex.getMessage(), ex.getCodeHTTP()))
                }
                break
            case 'PUT':
                try {
                    userEntityService.updateUser(currentUser, request.JSON)
                    render(responseService.createSuccessResponse(response, currentUser, 'User updated', 200))
                } catch (ApiException ex) {
                    render(responseService.createErrorResponse(response, ex.getMessage(), ex.getCodeHTTP()))
                }
                break
            case 'DELETE':
                try {
                    User userDeleted = userEntityService.deleteUser(currentUser)
                    render(responseService.createSuccessResponse(response, userDeleted, 'User deleted', 200))
                } catch (ApiException ex) {
                    render(responseService.createErrorResponse(response, ex.getMessage(), ex.getCodeHTTP()))
                }
                break
            default:
                render(responseService.createErrorResponse(response, 'Method not implemented', 405))
        }
    }

    def users() {
        User currentUser = springSecurityService.currentUser

        switch (request.getMethod()) {
            case 'GET':
                render(responseService.createSuccessResponse(response, User.findAll(), 'Users found', 200))
                break
            case 'POST':
                Role roleUser = Role.findByAuthority('ROLE_USER')
                try {
                    List<Message> listMessages = []
                    request.JSON.each {
                        listMessages.add(userEntityService.createUserWithDefaultRole(it, [roleUser.id.toString()]))
                    }
                    render(responseService.createSuccessResponse(response, listMessages, 'Users created', 201))
                } catch (ApiException ex) {
                    render(responseService.createErrorResponse(response, ex.getMessage(), ex.getCodeHTTP()))
                }
                break
        }
        render(responseService.createErrorResponse(response, 'Method not implemented', 405))
    }
}
