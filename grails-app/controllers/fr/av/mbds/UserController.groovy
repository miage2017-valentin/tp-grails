package fr.av.mbds

import fr.av.mbds.command.ProfileImageCommand
import fr.av.mbds.impl.UploadFileService
import fr.av.mbds.impl.UserEntityService
import grails.converters.JSON
import grails.plugin.springsecurity.SpringSecurityService
import grails.plugin.springsecurity.annotation.Secured
import grails.validation.ValidationException

import java.nio.file.NoSuchFileException

import static org.springframework.http.HttpStatus.*

@Secured('ROLE_ADMIN')
class UserController {
    UploadFileService uploadFileService
    UserService userService
    UserEntityService userEntityService
    SpringSecurityService springSecurityService

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE", restore: "PUT"]
    static private relativePathTmp = '/upload/user/tmp/'
    static private relativePathTarget = '/upload/user/'

    def index() {
        respond userService.list(params), model:[userCount: userService.count()]
    }

    def show(Long id) {
        respond userService.get(id)
    }

    def create() {
        respond new User(params)
    }

    def save(User user) {
        if (user == null) {
            notFound()
            return
        }

        try {
            userService.save(user)
            // Move image profile if exist
            String url = params.get('imageProfileUrl')
            if(url != ""){
                String imageFileUrl = uploadFileService.moveTmpFile(url, relativePathTmp, relativePathTarget)
                user = userService.updateProfileImage(user.id, user.version, imageFileUrl)
                user = userService.updateImageFile(user.id, user.version, url)
            }
        } catch (ValidationException e) {
            respond user.errors, view:'create'
            return
        } catch (NoSuchFileException e) {
            flash.message = message(code: 'default.file.not.found')
            respond user, view:'create'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'user.label', default: 'User'), user.id])
                redirect user
            }
            '*' { respond user, [status: CREATED] }
        }
    }

    def edit(Long id) {
        respond userService.get(id)
    }

    def update(User user) {
        if (user == null) {
            notFound()
            return
        }

        try {
            String url = params.get('imageProfileUrl')
            String[] roles = params.get('rolesSelected[]')
            userEntityService.assignRolesToUser(roles,user)
            userService.save(user)
            if(url != ""){
                String imageFileUrl = uploadFileService.moveTmpFile(url, relativePathTmp, relativePathTarget)
                user = userService.updateProfileImage(user.id, user.version, imageFileUrl)
                // delete old image
                if(user.imageFile != null) {
                    uploadFileService.removeFile(user.imageFile, relativePathTarget)
                }
                user = userService.updateImageFile(user.id, user.version, url)
            }

            def currentUser = springSecurityService.currentUser
            if(currentUser.username == user.username) {
                springSecurityService.reauthenticate(currentUser.username)
            }
        } catch (ValidationException e) {
            respond user.errors, view:'edit'
            return
        } catch (NoSuchFileException e) {
            flash.message = message(code: 'default.file.not.found')
            respond user, view:'edit'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'user.label', default: 'User'), user.id])
                redirect user
            }
            '*'{ respond user, [status: OK] }
        }
    }

    def delete(Long id) {
        if (id == null) {
            notFound()
            return
        }
        // Disable and lock user account
        User user = User.findById(id)
        user.enabled = false
        user.accountLocked = true
        userService.save(user)
        //userService.delete(id)

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'user.label', default: 'User'), id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    def restore(Long id) {
        if (id == null) {
            notFound()
            return
        }
        // Enable user
        User user = User.findById(id)
        user.enabled = true
        user.accountLocked = false
        userService.save(user)

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.restore.message', args: [message(code: 'user.label', default: 'User'), id])
                redirect action: "index", method: "GET"
            }
            '*' { render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'user.label', default: 'User'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }

    def editProfileImage(Long id) {
        User user = User.get(id)
        if (!user) {
            notFound()
        }
        [user: user]
    }

    def uploadProfileImage(ProfileImageCommand cmd) {

        if (cmd.hasErrors()) {
            //respond(cmd, model: [user: cmd], view: 'create')
            response.status = 400
            def error = cmd.getErrors().getFieldError("imageFile").getCode()
            render(['error': true, 'message': message(code: error)] as JSON)
        }

        String filename = uploadFileService.uploadFileInTemp(cmd, relativePathTmp)

        Locale locale = request.locale
        //flash.message = crudMessageService.message(CRUD.UPDATE, domainName(locale), pointOfInterest.id, locale)
        render(['filename':filename, 'url': uploadFileService.getUrl(filename, relativePathTmp)] as JSON)
    }
}
