package fr.av.mbds

import groovy.transform.EqualsAndHashCode
import groovy.transform.ToString
import grails.compiler.GrailsCompileStatic

@GrailsCompileStatic
@EqualsAndHashCode(includes='username')
@ToString(includes='username', includeNames=true, includePackage=false)
class User implements Serializable {

    private static final long serialVersionUID = 1

    String username
    String password
    boolean enabled = true
    boolean accountExpired
    boolean accountLocked
    boolean passwordExpired
    String pathProfileImage
    String imageFile

    static hasMany = [matchWon:Game,matchLost:Game, messageSent:Message, messageReceived: Message]
    static mappedBy = [matchWon:'winner', matchLost: 'looser', messageSent: 'author', messageReceived: 'target']

    Set<Role> getAuthorities() {
        (UserRole.findAllByUser(this) as List<UserRole>)*.role as Set<Role>
    }

    static constraints = {
        password nullable: false, blank: false, password: true
        username nullable: false, blank: false, unique: true
        pathProfileImage nullable: true
        imageFile nullable: true
    }

    static mapping = {
	    password column: '`password`'
    }

}
