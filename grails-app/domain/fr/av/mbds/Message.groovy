package fr.av.mbds

class Message {

    User author
    User target
    String content
    boolean isRead

    Date dateCreated

    static constraints = {
        target nullable: false
        author nullable: false
        content blank: false
        content  size: 2..15000
    }

    static mapping = {
        isRead defaultValue: false
        content sqlType: 'text'
    }


}
