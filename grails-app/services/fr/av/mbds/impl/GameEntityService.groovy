package fr.av.mbds.impl

import fr.av.mbds.Game
import fr.av.mbds.GameService
import fr.av.mbds.User
import fr.av.mbds.exceptions.ApiException
import grails.gorm.transactions.Transactional
import grails.plugin.springsecurity.SpringSecurityService
import grails.validation.ValidationException
import org.grails.web.json.JSONObject

@Transactional
class GameEntityService {

    SpringSecurityService springSecurityService
    GameService gameService


    def createGame(JSONObject params) throws ApiException {
        if (User.findAllByIdInList([params.winner, params.looser]).size() != 2) {
            throw new ApiException('User not found', 404);
        }
        Game newGame = new Game(params)
        try {
            gameService.save(newGame)
            return newGame
        } catch (ValidationException e) {
            throw new ApiException(e.getMessage(), 400)
        }
    }


}
