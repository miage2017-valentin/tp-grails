package fr.av.mbds.impl

import fr.av.mbds.Role
import fr.av.mbds.User
import fr.av.mbds.UserRole
import fr.av.mbds.exceptions.ApiException
import grails.gorm.transactions.Transactional
import grails.validation.ValidationException
import org.grails.web.json.JSONObject

@Transactional
class UserEntityService {

    /**
     * Method to assign roles on user selected
     * WARNING: Each roles that not exist in listIdRoles, it's deleted
     * @param listIdRoles
     * @param user
     */
    def assignRolesToUser(String[] listIdRoles, User user) {
        this.deleteAllRoleOfUser(user)
        for (String role : listIdRoles) {
            Role roleFind = Role.findById(role)
            UserRole userRole = new UserRole(user: user, role: roleFind)
            userRole.save(flush: true)
        }
    }

    def deleteAllRoleOfUser(User user) {
        for (UserRole userRole : UserRole.findAllByUser(user)) {
            userRole.delete(flush: true)
        }
    }

    @Transactional
    def createUserWithDefaultRole(JSONObject params, List<String> listIdRoles) throws ApiException {
        try {
            User user = new User(params)
            user.save()
            if (user.hasErrors()) {
                throw new ApiException(user.getErrors().getFieldError().toString(), 400)
            }
            this.assignRolesToUser(listIdRoles.toArray() as String[], user)
            return user
        } catch (ValidationException e) {
            throw new ApiException(e.getMessage(), 400)
        }
    }

    @Transactional
    def updateUser(User user, JSONObject params) throws ApiException {
        try {
            user.properties = params
            user.save()
            if (user.hasErrors()) {
                throw new ApiException(user.getErrors().getAllErrors().toString(), 400)
            }
            return user
        } catch (ValidationException e) {
            throw new ApiException(e.getMessage(), 400)
        }
    }

    @Transactional
    /**
     * Method to disable and lock user account
     */
    def deleteUser(User user) throws ApiException {
        try {
            user.enabled = false
            user.accountLocked = true
            user.save()
            if (user.hasErrors()) {
                throw new ApiException(user.getErrors().getAllErrors().toString(), 400)
            }
            return user
        } catch (Exception e) {
            throw new ApiException(e.getMessage(), 400)
        }
    }
}
