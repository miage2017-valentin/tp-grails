package fr.av.mbds.impl

import fr.av.mbds.Message
import fr.av.mbds.MessageService
import fr.av.mbds.User
import fr.av.mbds.exceptions.ApiException
import grails.gorm.transactions.Transactional
import grails.plugin.springsecurity.SpringSecurityService
import grails.validation.ValidationException
import org.grails.web.json.JSONObject

@Transactional
class MessageEntityService {

    SpringSecurityService springSecurityService
    MessageService messageService

    def createMessage(JSONObject params) throws ApiException {
        User currentUser = springSecurityService.currentUser
        User targetUser = User.findById(params.target)
        if (targetUser == null) {
            throw new ApiException('Target user not found', 404)
        }
        params.author = currentUser
        Message newMessage = new Message(params)
        try {
            messageService.save(newMessage)
            return newMessage
        } catch (ValidationException e) {
            throw new ApiException(e.getMessage(), 400)
        }
    }
}
