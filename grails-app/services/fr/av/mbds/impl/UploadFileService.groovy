package fr.av.mbds.impl

import fr.av.mbds.command.ProfileImageCommand
import grails.compiler.GrailsCompileStatic
import grails.config.Config
import grails.core.support.GrailsConfigurationAware

import java.nio.file.Files
import java.nio.file.NoSuchFileException
import java.nio.file.Path
import java.nio.file.Paths

@SuppressWarnings('GrailsStatelessService')
@GrailsCompileStatic
class UploadFileService implements GrailsConfigurationAware {

    String cdnFolder
    String cdnRootUrl

    @Override
    void setConfiguration(Config co) {
        cdnFolder = co.getRequiredProperty('grails.uploadServer.cdnFolder')
        cdnRootUrl = co.getRequiredProperty('grails.uploadServer.cdnRootUrl')
    }

    String uploadFileInTemp(ProfileImageCommand cmd, String tmpPath) {

        String extension = cmd.imageFile.getOriginalFilename().split("\\.")[1]
        String filename =  "${UUID.randomUUID()}.${extension}"
        File folder = Paths.get(cdnFolder, tmpPath).toFile()
        if ( !folder.exists() ) {
            folder.mkdirs()
        }
        String path = "${folder}/${filename}"
        cmd.imageFile.transferTo(new File(path))

        filename
    }

    String moveTmpFile(String filename, String tmpPath ,String destinationPath) throws NoSuchFileException{
        Path srcPath = Paths.get(cdnFolder,tmpPath, filename)
        Path targetPath = Paths.get(cdnFolder, destinationPath, filename)
        Files.move(srcPath, targetPath)

        String imageFileUrl = getUrl(filename, destinationPath)
        imageFileUrl
    }

    void removeFile(String filename, String path){
        Path targetPath = Paths.get(cdnFolder, path, filename)
        Files.delete(targetPath)
    }

    String getUrl(String filename, String subDirectory){
        URI uriRoot = new URI(cdnRootUrl)
        uriRoot = uriRoot.resolve(subDirectory)
        uriRoot = uriRoot.resolve(filename)
        return uriRoot
    }
}
