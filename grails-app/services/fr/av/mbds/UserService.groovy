package fr.av.mbds

import grails.gorm.services.Service

@Service(User)
interface UserService {

    User get(Serializable id)

    List<User> list(Map args)

    Long count()

    void delete(Serializable id)

    User save(User user)

    User updateProfileImage(Serializable id, Long version, String pathProfileImage)

    User updateImageFile(Serializable id, Long version, String imageFile)
}