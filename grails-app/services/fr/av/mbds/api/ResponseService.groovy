package fr.av.mbds.api

import grails.converters.JSON
import grails.gorm.transactions.Transactional

import javax.servlet.http.HttpServletResponse

@Transactional
class ResponseService {

    def createErrorResponse(HttpServletResponse response,String message= "Error", errorCode = 400){
        response.status = errorCode
        ['message':message] as JSON
    }

    def createSuccessResponse(HttpServletResponse response,data, String message = "Success", successCode = 200){
        response.status = successCode
        ['message':message, 'data':data] as JSON
    }
}
