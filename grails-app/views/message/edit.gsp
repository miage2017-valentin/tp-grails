<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="admin"/>
    <g:set var="entityName" value="${message(code: 'message.label', default: 'Message')}"/>
    <title><g:message code="default.edit.label" args="[entityName]"/></title>
</head>

<body>
<div class="container-fluid">
    <g:if test="${flash.message}">
        <div class="alert alert-success mx-auto col-10">
            <div>${flash.message}</div>
        </div>
    </g:if>
    <g:hasErrors bean="${this.game}">
        <div class="alert alert-danger mx-auto col-10">
            <ul>
                <g:eachError bean="${this.game}" var="error">
                    <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message
                            error="${error}"/></li>
                </g:eachError>
            </ul>
        </div>
    </g:hasErrors>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header"><g:message code="default.edit.label" args="[entityName]"/></div>
                <g:form resource="${this.message}" method="PUT">
                    <div class="card-body">
                        <g:hiddenField name="version" value="${this.message?.version}"/>
                        <f:all bean="message" except="isRead, content"/>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="content">content</label>

                            <div class="col-md-9">
                                <textarea class="form-control" id="content" name="content" rows="9"
                                          placeholder="Content..">${message.content}</textarea>
                            </div>
                        </div>
                    </div>

                    <div class="card-footer">
                        <button class="btn btn-sm btn-primary" type="submit">
                            <i class="fa fa-dot-circle-o"></i> ${message(code: 'default.button.update.label', default: 'Update')}
                        </button>
                        <button class="btn btn-sm btn-danger" type="reset">
                            <i class="fa fa-ban"></i> Reset</button>
                    </div>
                </g:form>
            </div>
        </div>
    </div>
</body>
</html>
