<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="admin"/>
    <g:set var="entityName" value="${message(code: 'message.label', default: 'Message')}"/>
    <title><g:message code="default.show.label" args="[entityName]"/></title>
</head>

<body>
<div class="container-fluid">
    <g:if test="${flash.message}">
        <div class="alert alert-success mx-auto col-10">
            <div>${flash.message}</div>
        </div>
    </g:if>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header"><g:message code="default.show.label" args="[entityName]"/>
                    <div class="float-right">
                        <a class="card-header-action delete-badge">
                            <g:form resource="${this.message}" method="DELETE">
                                <input type="hidden" name="_method" value="DELETE" id="_method"/>
                                <input class="badge badge-pill badge-danger delete" type="submit"
                                       value="<g:message code="default.button.delete.label"/>">
                            </g:form>
                        </a>
                    </div>

                    <div class="float-right">
                        <a class="card-header-action"
                           href="<g:createLink controller="message" action="edit" params="['id': message.id]"/>">
                            <small class="badge badge-pill badge-success edit"><g:message
                                    code="default.button.edit.label"/></small>
                        </a>
                    </div>
                </div>

                <div class="card-body">
                    <div id="rowMessageSend" class="row">
                        <div class="col-10 mx-auto">
                            <div class="avatar  index_game  img-avatar-message">
                                <g:if test="${message.author.pathProfileImage == null}">
                                    <g:img dir="images" file="user.svg" class="img-avatar"/>
                                </g:if>
                                <g:else>
                                    <img class="img-avatar" src="${message.author.pathProfileImage}"/>
                                </g:else>
                            </div>
                            ${message.author.username}
                            <g:img dir="images" file="arrow-pointing-to-right.svg" class="arrow-send"/>
                            <div class="avatar  index_game  img-avatar-message">
                                <g:if test="${message.target.pathProfileImage == null}">
                                    <g:img dir="images" file="user.svg" class="img-avatar"/>
                                </g:if>
                                <g:else>
                                    <img class="img-avatar" src="${message.target.pathProfileImage}"/>
                                </g:else>
                            </div>
                            ${message.target.username}
                        </div>
                    </div>

                    <div class="jumbotron jumbotron-fluid content-message">
                        <div class="container">
                            <h1 class="display-6">Content:</h1>

                            <p class="lead">${message.content}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
