<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="admin"/>
    <g:set var="entityName" value="${message(code: 'message.label', default: 'Message')}"/>
    <title><g:message code="default.list.label" args="[entityName]"/></title>
</head>

<body>
<div class="container-fluid">
    <g:if test="${flash.message}">
        <div class="alert alert-success mx-auto col-10">
            <div>${flash.message}</div>
        </div>
    </g:if>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header"><g:message code="default.list.label" args="[entityName]"/></div>

                <div class="card-body">
                    <table id="tableMessages" class="table table-responsive-sm table-hover table-outline mb-0">
                        <thead class="thead-light">
                        <tr>

                            <th>Author</th>
                            <th>Target</th>
                            <th>Content</th>
                            <th>Is read</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <g:each var="messageObj" in="${messageList}">
                            <tr>
                                <td>
                                    <div class="avatar  index_game">
                                        <g:if test="${messageObj.author.pathProfileImage == null}">
                                            <g:img dir="images" file="user.svg" class="img-avatar"/>
                                        </g:if>
                                        <g:else>
                                            <img class="img-avatar" src="${messageObj.author.pathProfileImage}"/>
                                        </g:else>
                                    </div>
                                    ${messageObj.author.username}
                                </td>
                                <td>
                                    <div class="avatar  index_game">
                                        <g:if test="${messageObj.target.pathProfileImage == null}">
                                            <g:img dir="images" file="user.svg" class="img-avatar"/>
                                        </g:if>
                                        <g:else>
                                            <img class="img-avatar" src="${messageObj.target.pathProfileImage}"/>
                                        </g:else>
                                    </div>
                                    ${messageObj.target.username}
                                </td>
                                <td>
                                    ${messageObj.content.take(20)}
                                    <g:if test="${messageObj.content.length() > 20}">...</g:if>
                                </td>
                                <td>
                                    <g:if test="${messageObj.isRead}">
                                        <i class="icon-check"></i>
                                    </g:if>
                                </td>
                                <td>
                                    <div class="row">
                                        <div class="col-4 col-sm-12 col-md-12 col-xl-4">
                                            <a href="<g:createLink controller="message" action="show"
                                                                   params="[id: messageObj.id]"/>">
                                                <button class="btn btn-block btn-primary" type="button"><g:message
                                                        code="gui.button.learnMore"/></button>
                                            </a>
                                        </div>

                                        <div class="col-4 col-sm-12 col-md-12 col-xl-4">
                                            <a href="<g:createLink controller="message" action="edit"
                                                                   params="[id: messageObj.id]"/>">
                                                <button class="btn btn-block btn-success" type="button"><g:message
                                                        code="default.button.edit.label"/></button>
                                            </a>
                                        </div>

                                        <div class="col-4 col-sm-12 col-md-12 col-xl-4">
                                            <form method="post"
                                                  action="<g:createLink controller="message" action="delete"
                                                                        params="[id: messageObj.id]"/>"
                                                  onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');">
                                                <input type="hidden" name="_method" value="DELETE" id="_method"/>
                                                <input class="btn btn-block btn-danger" type="submit"
                                                       value="<g:message code="default.button.delete.label"/>"/>
                                            </form>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        </g:each>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    document.addEventListener("DOMContentLoaded", function () {
        $('#tableMessages').DataTable({
            responsive: true,
        });
    });
</script>
</body>
</html>