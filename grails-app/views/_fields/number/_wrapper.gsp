<div class="form-group row">
    <label class="col-md-3 col-form-label" for="${property}">${label}${required ? '*' : ''}</label>

    <div class="col-md-3">
        <input class="form-control" id="${property}" type="number" name="${(prefix ?: '') + property}" value="${value}"
               <g:if test="${required}">required=""</g:if>/>
    </div>
</div>