<div class="form-group row">
    <label class="col-md-3 col-form-label" for="${property}">${label}${required ? '*' : ''}</label>

    <div class="col-md-6">
        <input class="form-control" id="${property}" type="password" name="${(prefix ?: '') + property}"
               <g:if test="${required}">required=""</g:if>/>
    </div>
</div>