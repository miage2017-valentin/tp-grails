<div class="form-group row">
    <label class="col-md-3 col-form-label" for="${property}">${label}${required ? '*' : ''}</label>

    <div class="col-md-9 col-form-label">
        <div class="form-check checkbox">
            <input type="hidden" name="_${(prefix ?: '') + property}">
            <input class="form-check-input" id="${property}" type="checkbox" name="${(prefix ?: '') + property}"
                <g:if test="${value}">
                    checked
                    value="true"
                </g:if>
                <g:else>
                    value="false"
                </g:else>
                   <g:if test="${required}">required=""</g:if>>
        </div>
    </div>
</div>