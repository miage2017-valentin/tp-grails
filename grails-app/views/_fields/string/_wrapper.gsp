<div class="form-group row">
    <label class="col-md-3 col-form-label" for="${property}">${label}${required ? '*' : ''}</label>

    <div class="col-md-6">
        <input class="form-control" id="${property}" type="text" name="${(prefix ?: '') + property}" value="${value}"
               <g:if test="${required}">required=""</g:if>/>
    </div>
</div>