<div class="form-group row">
    <label class="col-md-3 col-form-label" for="${property}">${property}</label>
    <div class="col-md-9">
        <select class="form-control" id="${property}" name="${property}">
            <g:if test="${constraints.isNullable()}">
                <option value=""><g:message code="fields.noValue"/></option>
            </g:if>
            <g:each var="user" in="${type.findAll()}">
                <option value="${user.id}">${user.username}</option>
            </g:each>
        </select>
    </div>
</div>