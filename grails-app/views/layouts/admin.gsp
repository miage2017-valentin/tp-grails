<!doctype html>
<html lang="en" class="no-js">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <title>
        <g:layoutTitle default="Projet Grails"/>
    </title>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <asset:link rel="icon" href="favicon.ico" type="image/x-ico"/>

    <asset:stylesheet src="admin/bootstrap.min.css"/>
    <asset:stylesheet src="admin/coreui-icons.min.css"/>
    <asset:stylesheet src="admin/flag-icon.min.css"/>
    <asset:stylesheet src="admin/simple-line-icons.css"/>
    <asset:stylesheet src="admin/style.min.css"/>
    <asset:stylesheet src="datatables.min.css"/>
    <asset:stylesheet src="styles.css"/>
    <g:layoutHead/>
</head>

<body class="app header-fixed sidebar-fixed aside-menu-fixed sidebar-lg-show">
<header class="app-header navbar">
    <button class="navbar-toggler sidebar-toggler d-lg-none mr-auto" type="button" data-toggle="sidebar-show">
        <span class="navbar-toggler-icon"></span>
    </button>
    <a class="navbar-brand" href="#">
        <g:img dir="images" file="mbds_logo" class="navbar-brand-full" width="89" height="89" alt="MBDS Logo"/>
        <g:img dir="images" file="mbds_logo" class="navbar-brand-minimized" width="30" height="30" alt="MBDS Logo"/>
    </a>
    <button class="navbar-toggler sidebar-toggler d-md-down-none" type="button" data-toggle="sidebar-lg-show">
        <span class="navbar-toggler-icon"></span>
    </button>
    <ul class="nav navbar-nav ml-auto">
        <li class="nav-item dropdown">
            <a class="nav-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true"
               aria-expanded="false">
                <g:message code="default.message.welcome" args="${sec.username()}"/>
                <g:set var="url" value="${sec.loggedInUserInfo([field:'pathProfileImage'])}"/>
                <g:set var="idCurrentUser" value="${sec.loggedInUserInfo([field: 'id'])}"/>
                <g:if test="${url == ''}">
                    <g:img dir="images" file="user.svg" class="img-avatar"/>
                </g:if>
                <g:else>
                    <img class="img-avatar" src="${url}"/>
                </g:else>
            </a>

            <div class="dropdown-menu dropdown-menu-right">
                <div class="dropdown-header text-center">
                    <strong>Settings</strong>
                </div>
                <a class="dropdown-item"
                   href="<g:createLink controller="user" action="show" params="[id: idCurrentUser]"/>">
                    <i class="fa fa-user"></i> Profile</a>
                <a class="dropdown-item"
                   href="<g:createLink controller="user" action="edit" params="[id: idCurrentUser]"/>">
                    <i class="fa fa-wrench"></i> Settings</a>
                </a>

                <div class="divider"></div>
                <a class="dropdown-item" href="<g:createLink controller='logout'/>">
                    <i class="fa fa-lock"></i> <g:message code="gui.button.logout"/></a>
            </div>
        </li>
    </ul>
</header>

<div class="app-body">
    <div class="sidebar">
        <nav class="sidebar-nav">
            <ul class="nav">
                <li class="nav-item">
                    <a class="nav-link" href="<g:createLink controller="admin" action="index" />">
                        <i class="nav-icon icon-speedometer"></i> <g:message code="nav.dashboard"/>
                    </a>
                </li>
                <li class="nav-title"><g:message code="nav.usersAndRole" /></li>
                <li class="nav-item nav-dropdown">
                    <a class="nav-link nav-dropdown-toggle" href="#">
                        <i class="nav-icon icon-people"></i> <g:message code="nav.users"/></a>
                    <ul class="nav-dropdown-items">
                        <li class="nav-item">
                            <a class="nav-link" href="<g:createLink controller="user" action="index" />">
                                <i class="nav-icon icon-list"></i> <g:message code="default.list.label" args="['Users']"/></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="<g:createLink controller="user" action="create" />">
                                <i class="nav-icon icon-plus"></i> <g:message code="default.create.label" args="['User']"/></a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item nav-dropdown">
                    <a class="nav-link nav-dropdown-toggle" href="#">
                        <i class="nav-icon icon-key"></i> <g:message code="nav.roles"/></a>
                    <ul class="nav-dropdown-items">
                        <li class="nav-item">
                            <a class="nav-link" href="<g:createLink controller="role" action="index" />">
                                <i class="nav-icon icon-list"></i> <g:message code="default.list.label" args="['Roles']"/></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="<g:createLink controller="role" action="create" />">
                                <i class="nav-icon icon-plus"></i> <g:message code="default.create.label" args="['Role']"/></a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item nav-dropdown">
                    <a class="nav-link nav-dropdown-toggle" href="#">
                        <i class="nav-icon icon-game-controller"></i> <g:message code="nav.games"/></a>
                    <ul class="nav-dropdown-items">
                        <li class="nav-item">
                            <a class="nav-link" href="<g:createLink controller="game" action="index" />">
                                <i class="nav-icon icon-list"></i> <g:message code="default.list.label" args="['Games']"/></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="<g:createLink controller="game" action="create" />">
                                <i class="nav-icon icon-plus"></i> <g:message code="default.create.label" args="['Game']"/></a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item nav-dropdown">
                    <a class="nav-link nav-dropdown-toggle" href="#">
                        <i class="nav-icon icon-envelope-letter"></i> <g:message code="nav.messages"/></a>
                    <ul class="nav-dropdown-items">
                        <li class="nav-item">
                            <a class="nav-link" href="<g:createLink controller="message" action="index" />">
                                <i class="nav-icon icon-list"></i> <g:message code="default.list.label" args="['Messages']"/></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="<g:createLink controller="message" action="create" />">
                                <i class="nav-icon icon-plus"></i> <g:message code="default.create.label" args="['Message']"/></a>
                        </li>
                    </ul>
                </li>
            </ul>
        </nav>
        <button class="sidebar-minimizer brand-minimizer" type="button"></button>
    </div>
    <main class="main">
        <g:layoutBody/>
    </main>
</div>
<footer class="app-footer">
    <div>
        <span>&copy; 2018 AV</span>
    </div>
</footer>

<asset:javascript src="admin/jquery.min.js"/>
<asset:javascript src="admin/popper.min.js"/>
<asset:javascript src="admin/bootstrap.min.js"/>
<asset:javascript src="admin/coreui.min.js"/>
<asset:javascript src="admin/pace.min.js"/>
<asset:javascript src="admin/perfect-scrollbar.min.js"/>
<asset:javascript src="datatables.min.js"/>
<asset:javascript src="admin/custom-tooltips.min.js"/>
</body>
</html>
