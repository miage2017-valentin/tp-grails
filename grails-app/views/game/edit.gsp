<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="admin"/>
    <g:set var="entityName" value="${message(code: 'game.label', default: 'Game')}"/>
    <title><g:message code="default.edit.label" args="[entityName]"/></title>
</head>

<body>
<div class="container-fluid">
    <g:if test="${flash.message}">
        <div class="alert alert-success mx-auto col-10">
            <div>${flash.message}</div>
        </div>
    </g:if>
    <g:hasErrors bean="${this.user}">
        <div class="alert alert-danger mx-auto col-10">
            <ul>
                <g:eachError bean="${this.user}" var="error">
                    <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message
                            error="${error}"/></li>
                </g:eachError>
            </ul>
        </div>
    </g:hasErrors>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header"><g:message code="default.information.label" args="[entityName]" /></div>
                <g:form resource="${this.game}" method="PUT">
                    <div class="card-body">

                        <g:hiddenField name="version" value="${this.game?.version}"/>
                        <fieldset class="form">
                            <f:all bean="game"/>
                        </fieldset>
                    </div>

                    <div class="card-footer">
                        <button class="btn btn-sm btn-primary" type="submit">
                            <i class="fa fa-dot-circle-o"></i> ${message(code: 'default.button.update.label', default: 'Update')}
                        </button>
                        <button class="btn btn-sm btn-danger" type="reset">
                            <i class="fa fa-ban"></i> Reset</button>
                    </div>
                </g:form>
            </div>
        </div>
    </div>
</div>
</div>
</body>
</html>
