<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="admin"/>
    <g:set var="entityName" value="${message(code: 'game.label', default: 'Game')}"/>
    <title><g:message code="default.show.label" args="[entityName]"/></title>
</head>

<body>
<div class="container-fluid">
    <g:if test="${flash.message}">
        <div class="alert alert-success mx-auto col-10">
            <div>${flash.message}</div>
        </div>
    </g:if>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header"><g:message code="default.information.label" args="['Game']"/>
                    <div class="float-right">
                        <a class="card-header-action delete-badge">
                            <g:form resource="${this.game}" method="DELETE">
                                <input type="hidden" name="_method" value="DELETE" id="_method"/>
                                <input class="badge badge-pill badge-danger delete" type="submit"
                                       value="<g:message code="default.button.delete.label"/>">
                            </g:form>
                        </a>
                    </div>

                    <div class="float-right">
                        <a class="card-header-action"
                           href="<g:createLink controller="game" action="edit" params="['id': this.game.id]"/>">
                            <small class="badge badge-pill badge-success edit">Edit</small>
                        </a>
                    </div>
                </div>

                <div class="card-body">
                    <div>
                        <p>Winner : ${game.winner.username}</p>

                        <p>Looser : ${game.looser.username}</p>

                        <p>Winner Score: ${game.winnerScore}</p>

                        <p>Looser Score :${game.looserScore}</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>
