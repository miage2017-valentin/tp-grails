<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="admin"/>
    <g:set var="entityName" value="${message(code: 'game.label', default: 'Games')}"/>
    <title><g:message code="default.list.label" args="[entityName]"/></title>
</head>

<body>
<div class="container-fluid">
    <g:if test="${flash.message}">
        <div class="alert alert-success mx-auto col-10">
            <div>${flash.message}</div>
        </div>
    </g:if>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header"><g:message code="default.list.label" args="[entityName]"/></div>

                <div class="card-body">
                    <table id="tableGames" class="table table-responsive-sm table-hover table-outline mb-0">
                        <thead class="thead-light">
                        <tr>

                            <th>Winner</th>
                            <th>Looser</th>
                            <th>Winner score</th>
                            <th>Looser score</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <g:each var="game" in="${gameList}">
                            <tr>
                                <td>
                                    <div class="avatar  index_game">
                                        <g:if test="${game.winner.pathProfileImage == null}">
                                            <g:img dir="images" file="user.svg" class="img-avatar"/>
                                        </g:if>
                                        <g:else>
                                            <img class="img-avatar" src="${game.winner.pathProfileImage}"/>
                                        </g:else>
                                    </div>
                                    ${game.winner.username}
                                </td>
                                <td>
                                    <div class="avatar  index_game">
                                        <g:if test="${game.looser.pathProfileImage == null}">
                                            <g:img dir="images" file="user.svg" class="img-avatar"/>
                                        </g:if>
                                        <g:else>
                                            <img class="img-avatar" src="${game.looser.pathProfileImage}"/>
                                        </g:else>
                                    </div>
                                    ${game.looser.username}
                                </td>
                                <td>${game.winnerScore}</td>
                                <td>${game.looserScore}</td>
                                <td>
                                    <div class="row">
                                        <div class="col-4 col-sm-4 col-md-4 col-xl-4">
                                            <a href="<g:createLink controller="game" action="show"
                                                                   params="[id: game.id]"/>">
                                                <button class="btn btn-block btn-primary" type="button"><g:message
                                                        code="default.button.show.label"/></button>
                                            </a>
                                        </div>

                                        <div class="col-4 col-sm-4 col-md-4 col-xl-4">
                                            <a href="<g:createLink controller="game" action="edit"
                                                                   params="[id: game.id]"/>">
                                                <button class="btn btn-block btn-success" type="button"><g:message
                                                        code="default.button.edit.label"/></button>
                                            </a>
                                        </div>

                                        <div class="col-4 col-sm-4 col-md-4 col-xl-4">
                                            <form method="post" action="<g:createLink controller="game" action="delete"
                                                                                      params="[id: game.id]"/>"
                                                  onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');">
                                                <input type="hidden" name="_method" value="DELETE" id="_method"/>
                                                <input class="btn btn-block btn-danger" type="submit"
                                                       value="<g:message code="default.button.delete.label"/>"/>
                                            </form>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        </g:each>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    document.addEventListener("DOMContentLoaded", function () {
        $('#tableGames').DataTable({
            responsive: true,
        });
    });
</script>
</body>
</html>