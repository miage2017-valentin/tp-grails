<html>
<head>
    <meta name="layout" content="${gspLayout ?: 'main'}"/>
    <title><g:message code='springSecurity.login.title'/></title>
    <asset:stylesheet src="login.css"/>
    </head>

    <body>
             <div class="wrapper">
                    %{--<div class="fheader"><g:message code='springSecurity.login.header'/></div>--}%


                <form action="${postUrl ?: '/login/authenticate'}" method="POST" class="form-signin" autocomplete="off">
                    <h2 class="form-signin-heading"><g:message code='login.message'/></h2>
                 <g:if test='${flash.message}'>
                     <div class="alert alert-danger">
                         <div id="msg" class="login_message">${flash.message}</div>
                     </div>
                 </g:if>
                    <p>
                        <label for="username"><g:message code='springSecurity.login.username.label'/>:</label>
                        <input type="text" class="form-control" name="${usernameParameter ?: 'username'}" placeholder="Username" id="username"/>
                    </p>

                    <p>
                        <label for="password"><g:message code='springSecurity.login.password.label'/>:</label>
                        <input type="password" class="form-control" name="${passwordParameter ?: 'password'}" placeholder="Password" id="password"/>
                    </p>

                    <p id="remember_me_holder">
                        <label class="checkbox">
                            <input type="checkbox" name="${rememberMeParameter ?: 'remember-me'}" id="remember_me"> <g:if test='${hasCookie}'>checked="checked"</g:if>
                            <g:message code='springSecurity.login.remember.me.label'/>
                        </label>
                    </p>

                    <p>
                        <button class="btn btn-lg btn-primary btn-block" type="submit" value="${message(code: 'springSecurity.login.button')}" >Login</button>
                </form>
             </div>

    </body>
</html>







