<!DOCTYPE html>

<html>
<head>
    <meta name="layout" content="admin"/>
    <g:set var="entityName" value="${message(code: 'user.label', default: 'User')}"/>
    <title><g:message code="default.show.label" args="[entityName]"/></title>
</head>

<body>
%{--<f:display bean="user" except="['pathProfileImage', 'password']"/>--}%

    <div class="container-fluid">
        <g:if test="${flash.message}">
            <div class="alert alert-success mx-auto col-10">
                <div>${flash.message}</div>
            </div>
        </g:if>
        <div class="rounded mx-auto d-block">
            <div id="previewProfileImage" class="avatarUpload circle imgProfil"
                 style="background-image:url(${this.user.pathProfileImage ?: resource(dir: 'images', file: 'user.svg')});">
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">User's Informations
                        <div class="float-right">
                            <a class="card-header-action delete-badge">
                                <g:form resource="${this.user}" method="DELETE">
                                    <input type="hidden" name="_method" value="DELETE" id="_method"/>
                                    <input class="badge badge-pill badge-danger delete" type="submit"
                                           value="<g:message code="default.button.delete.label"/>">
                                </g:form>
                            </a>
                        </div>
                        <div class="float-right">
                            <a class="card-header-action" href="<g:createLink controller="user" action="edit" params="['id':this.user.id]"/>">
                                <small class="badge badge-pill badge-success edit">Edit</small>
                            </a>
                        </div>
                    </div>


                    <div class="card-body">
                        <div>
                            <p>Name : ${user.username}</p>

                            <p>Enabled : ${user.enabled}</p>

                            <p>Account Expired : ${user.accountExpired}</p>

                            <p>AccountLocked :${user.accountLocked}</p>

                            <p>Password Expired : ${user.passwordExpired}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Won Games</div>

                    <div class="card-body">
                        <table id="tableGamesWon" class="table table-responsive-sm table-hover table-outline mb-0">
                            <thead class="thead-light">
                            <tr>
                                <th>Match number</th>
                                <th>Competitor</th>
                                <th>His score</th>
                                <th>Competitor's score</th>
                            </tr>
                            </thead>
                            <tbody>
                            <g:each var="match" in="${user.matchWon}">
                                <tr>
                                    <td>${match.id}</td>
                                    <td>${match.looser.username}</td>
                                    <td>${match.winnerScore}</td>
                                    <td>${match.looserScore}</td>
                                </tr>
                            </g:each>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>


        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Lost Games</div>

                        <div class="card-body">
                            <table id="tableGamesLoose" class="table table-responsive-sm table-hover table-outline mb-0">
                                <thead class="thead-light">
                                <tr>
                                    <th>Match number</th>
                                    <th>Competitor</th>
                                    <th>His score</th>
                                    <th>Competitor's score</th>
                                </tr>
                                </thead>
                                <tbody>
                                <g:each var="match" in="${user.matchLost}">
                                    <tr>
                                        <td>${match.id}</td>
                                        <td>${match.winner.username}</td>
                                        <td>${match.winnerScore}</td>
                                        <td>${match.looserScore}</td>
                                    </tr>
                                </g:each>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>



    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Sent Messages</div>

                <div class="card-body">
                    <table id="tableMessagesSent" class="table table-responsive-sm table-hover table-outline mb-0">
                        <thead class="thead-light">
                        <tr>
                            <th>Date</th>
                            <th>Receiver</th>
                            <th>Content</th>
                            <th>Is Read</th>
                            </tr>
                            </thead>
                            <tbody>
                            <g:each var="message" in="${user.messageSent}">
                                <tr>
                                    <td>${message.dateCreated}</td>
                                    <td>${message.target.username}</td>
                                    <td>${message.content}</td>
                                    <td>${message.isRead}</td>
                                </tr>
                            </g:each>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>



    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Received Messages</div>

                <div class="card-body">
                    <table id="tableMessagesReceived" class="table table-responsive-sm table-hover table-outline mb-0">
                        <thead class="thead-light">
                        <tr>
                            <th>Date</th>
                            <th>Author</th>
                            <th>Content</th>
                            <th>Is Read</th>
                        </tr>
                            </thead>
                            <tbody>
                            <g:each var="message" in="${user.messageReceived}">
                                <tr>
                                    <td>${message.dateCreated}</td>
                                    <td>${message.author.username}</td>
                                    <td>${message.content}</td>
                                    <td>${message.isRead}</td>
                                </tr>
                            </g:each>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

    </div>


</div>

<script>
    document.addEventListener("DOMContentLoaded", function () {
        $('#tableGamesWon').DataTable({
            responsive: true,
        });
    });

    document.addEventListener("DOMContentLoaded", function () {
        $('#tableGamesLoose').DataTable({
            responsive: true,
        });
    });

    document.addEventListener("DOMContentLoaded", function () {
        $('#tableMessagesSent').DataTable({
            responsive: true,
        });
    });

    document.addEventListener("DOMContentLoaded", function () {
        $('#tableMessagesReceived').DataTable({
            responsive: true,
        });
    });

</script>
</body>
</html>
