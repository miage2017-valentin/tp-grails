<g:set var="roles" value="${fr.av.mbds.Role.findAll()}"/>
<div class="form-group row">
    <label class="col-md-3 col-form-label">Roles</label>

    <div class="col-md-9 col-form-label">
        <g:each var="role" in="${roles}">
            <div class="form-check">
                <input id="roleSelected_${role.authority}" class="form-check-input" name="rolesSelected[]" type="checkbox"
                       value="${role.id}"
                    <g:if test="${user.isAttached()}">
                        <g:each var="userRole" in="${user.getAuthorities()}">
                            <g:if test="${role.id == userRole.id}">
                                checked
                            </g:if>
                        </g:each>
                    </g:if>>
                <label class="form-check-label" for="roleSelected_${role.authority}">${role.authority}</label>
            </div>
        </g:each>

    </div>
</div>