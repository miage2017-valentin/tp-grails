<div id="uploadErrorMessage" class="alert alert-danger hidden" role="alert" style="display:none;">
</div>
<div id="previewProfileImage" class="avatarUpload circle"
     style="background-image:url(${this.user.pathProfileImage ?: resource(dir:'images', file:'plus-black-symbol.svg')});
     <g:if test="${this.user.pathProfileImage == null}">
         background-size: 60%;
     </g:if>
     ">
    <input id="imageFile" type="file" name="imageFile"/>

    <div class="loading" style="display:none;">Loading&#8230;</div>
</div>

<asset:javascript src="jquery-2.2.0.min.js"/>
<script>
    $('#previewProfileImage').on('drag dragstart dragend dragover dragenter dragleave drop', function(e) {
        e.preventDefault();
        e.stopPropagation();
    })
        .on('dragover dragenter', function() {
            $(this).addClass('is-dragover');
        })
        .on('dragleave dragend drop', function() {
            $(this).removeClass('is-dragover');
        })
        .on('drop', function(e) {
            let droppedFiles = e.originalEvent.dataTransfer.files;
            if(droppedFiles) {
                sendImage(droppedFiles[0]);
            }
        });

    $('#imageFile').change(function(){
        sendImage($('#imageFile')[0].files[0]);
    });

    function sendImage(file){
        let formulaire = new FormData();
        formulaire.append('imageFile', file);
        $('#uploadErrorMessage').hide();
        $('.loading').show();
        $.ajax({
            url: '${createLink(controller: 'user', action: 'uploadProfileImage')}',
            type: 'POST',
            success: function(data){
                $('#imageProfileUrl').val(data.filename);
                $('#previewProfileImage').css("background-image", "url("+data.url+")");
                $('#previewProfileImage').css("background-size", "cover");
            },
            error: function(data){
                $('#imageFile').val('');
                $('#uploadErrorMessage').text(data.responseJSON.message);
                $('#uploadErrorMessage').show();
            },
            complete: function(data){
                $('.loading').hide();
            },
            data: formulaire,
            cache: false,
            contentType: false,
            processData: false,
            enctype: 'multipart/form-data'
        });
    }
</script>
<asset:stylesheet src="styles.css"/>