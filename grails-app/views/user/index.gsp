<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="admin"/>
    <g:set var="entityName" value="${message(code: 'user.label', default: 'User')}"/>
    <title><g:message code="default.list.label" args="['Users']"/></title>
</head>

<body>
<div class="container-fluid">
    <g:if test="${flash.message}">
        <div class="alert alert-success mx-auto col-10">
            <div>${flash.message}</div>
        </div>
    </g:if>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header"><g:message code="default.list.label" args="['Users']"/></div>

                <div class="card-body">
                    <table id="tableUsers" class="table table-responsive-sm table-hover table-outline mb-0">
                        <thead class="thead-light">
                        <tr>
                            <th class="text-center">
                                <i class="icon-people"></i>
                            </th>
                            <th>User</th>
                            <th>Status</th>
                            <th>Role</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <g:each var="user" in="${userList}">
                            <tr>
                                <td class="text-center">
                                    <div class="avatar">
                                        <g:if test="${user.pathProfileImage == null}">
                                            <g:img dir="images" file="user.svg" class="img-avatar"/>
                                        </g:if>
                                        <g:else>
                                            <img class="img-avatar" src="${user.pathProfileImage}"/>
                                        </g:else>
                                    </div>
                                </td>
                                <td>
                                    <div>${user.username}</div>
                                </td>
                                <td>
                                    <g:if test="${user.enabled && !user.accountLocked}">
                                        <span class="badge badge-success">Active</span>
                                    </g:if>
                                    <g:else>
                                        <span class="badge badge-danger">Deleted</span>
                                    </g:else>
                                </td>
                                <td>
                                    <g:each var="role" in="${user.getAuthorities()}">
                                        <span class="badge badge-dark">${role.authority}</span>
                                    </g:each>
                                </td>
                                <td>
                                    <div class="row">
                                        <div class="col-4 col-sm-4 col-md-4 col-xl-4">
                                            <a href="<g:createLink controller="user" action="show" params="[id:user.id]" />">
                                                <button class="btn btn-block btn-primary" type="button"><g:message
                                                        code="default.button.show.label"/></button>
                                            </a>
                                        </div>
                                        <div class="col-4 col-sm-4 col-md-4 col-xl-4">
                                            <a href="<g:createLink controller="user" action="edit" params="[id:user.id]" />">
                                                <button class="btn btn-block btn-success" type="button"><g:message code="default.button.edit.label"/></button>
                                            </a>
                                        </div>
                                        <div class="col-4 col-sm-4 col-md-4 col-xl-4">
                                            <g:if test="${user.enabled && !user.accountLocked}">
                                                <form method="post"
                                                      action="<g:createLink controller="user" action="delete"
                                                                            params="[id: user.id]"/>">
                                                    <input type="hidden" name="_method" value="DELETE" id="_method"/>
                                                    <input class="btn btn-block btn-danger" type="submit"
                                                           value="<g:message code="default.button.delete.label"/>"/>
                                                </form>
                                            </g:if>
                                            <g:else>
                                                <form method="post"
                                                      action="<g:createLink controller="user" action="restore"
                                                                            params="[id: user.id]"/>">
                                                    <input type="hidden" name="_method" value="PUT" id="_method"/>
                                                    <input class="btn btn-block btn-secondary" type="submit"
                                                           value="<g:message code="default.button.restore.label"/>"/>
                                                </form>
                                            </g:else>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        </g:each>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    document.addEventListener("DOMContentLoaded", function () {
        $('#tableUsers').DataTable({
            responsive: true,
        });
    });
</script>
</body>
</html>