<!DOCTYPE html>
<html>
    <head>
        <meta name="layout" content="admin" />
        <g:set var="entityName" value="${message(code: 'role.label', default: 'Role')}" />
        <title><g:message code="default.show.label" args="[entityName]" /></title>
    </head>
    <body>
            <div class="container-fluid">
                <g:if test="${flash.message}">
                    <div class="alert alert-success mx-auto col-10">
                        <div>${flash.message}</div>
                    </div>
                </g:if>
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header"><g:message code="default.information.label" args="['Role']"/>
                                <div class="float-right">
                                    <a class="card-header-action delete-badge">
                                        <g:form resource="${this.role}" method="DELETE">
                                            <input type="hidden" name="_method" value="DELETE" id="_method"/>
                                            <input class="badge badge-pill badge-danger delete" type="submit"
                                                   value="<g:message code="default.button.delete.label"/>">
                                        </g:form>
                                    </a>
                                </div>
                                <div class="float-right">
                                    <a class="card-header-action" href="<g:createLink controller="role" action="edit" params="['id':role.id]"/>">
                                        <small class="badge badge-pill badge-success edit">Edit</small>
                                    </a>
                                </div>
                            </div>
                            <div class="card-body">
                                <f:display bean="role" />
                                <table id="tableRole_${role.id}" class="table table-responsive-sm table-hover table-outline mb-0">
                                    <thead class="thead-light">
                                    <tr>
                                        <th>Users</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <g:each var="user" in="${role.getUsers()}">
                                        <tr>
                                            <td>${user.username}</td>
                                            <td>
                                                <div class="row">
                                                    <div class="col-4 col-sm-4 col-md-4 col-xl-4 rounded mx-auto d-block">
                                                        <form method="post" action="<g:createLink controller="user" action="delete" params="[id:user.id]" />">
                                                            <input type="hidden" name="_method" value="DELETE" id="_method"/>
                                                            <input class="btn btn-block btn-danger" type="submit" value="<g:message code="default.button.delete.label"/>" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');"/>
                                                        </form>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>

                                    </g:each>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <script>
        document.addEventListener("DOMContentLoaded", function () {
            $('#tableRole_${role.id}').DataTable({
                responsive: true,
            });
        });
    </script>
    </body>
</html>
