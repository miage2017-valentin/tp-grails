<!doctype html>
<html>
<head>
    <meta name="layout" content="admin"/>
    <title>Welcome to Grails</title>
</head>

<body>
<div class="container-fluid">

        <div class="row">
            <div class="col-sm-12 col-xl-12">
                <div class="card">
                    <div class="card-header">
                        <i class="fa fa-align-justify"></i> Welcome
                    </div>
                    <div class="card-body">
                        <div class="jumbotron">
                            <h1 class="display-3">Welcome to the admin part</h1>
                            <p class="lead">From this interface, you will be able to manage all the elements of the game.</p>
                            <p class="lead">
                                <a class="btn btn-primary btn-lg" href="#" role="button"><g:message code="gui.button.learnMore" /></a>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

</body>
</html>
