package tp_grails

import fr.av.mbds.*

class BootStrap {

    def init = { servletContext ->
        if(User.count() == 0) {
            def userRoleAdmin = new Role(authority:'ROLE_ADMIN').save()
            def userRole = new Role(authority:'ROLE_USER').save()

            def admin = new User(username: 'admin', password: 'admin').save(flush: true, failOnError: true)
            def user2 = new User(username: 'valentin', password:'valentin').save(flush:true, failOnError:true)
            def user_admin = new User(username: 'user_admin', password: 'test').save(flush:true, failOnError:true)
            def alison = new User(username: 'alison', password: 'alison').save(flush: true, failOnError: true)
            def gali = new User(username: 'gali', password: 'gali').save(flush: true, failOnError: true)

            UserRole.create(admin, userRoleAdmin)
            UserRole.create(admin, userRole)
            UserRole.create(user2, userRole)
            UserRole.create(user_admin, userRoleAdmin)
            UserRole.create(alison, userRole)
            UserRole.create(gali, userRole)
            UserRole.create(gali, userRoleAdmin)

            UserRole.withSession {
                it.flush()
                it.clear()
            }

            new Game(winner: admin, looser: user2, winnerScore: 200, looserScore: 3).save(flush: true, failOnError: true)
            new Game(winner: user2, looser: admin, winnerScore: 400, looserScore: 2).save(flush: true, failOnError: true)
            new Game(winner: user_admin, looser: admin, winnerScore: 400, looserScore: 2).save(flush: true, failOnError: true)
            new Game(winner: admin, looser: alison, winnerScore: 20, looserScore: 67).save(flush: true, failOnError: true)
            new Game(winner: gali, looser: alison, winnerScore: 30, looserScore: 90).save(flush: true, failOnError: true)


            new Message(author: admin, target: user2, content: "a supprimer", isRead: true).save(flush: true, failOnError: true)
            new Message(author: admin, target: user2, content: "a garder", isRead: false).save(flush: true, failOnError: true)
            new Message(author: admin, target: user2, content: "a supprimer 2", isRead: true).save(flush: true, failOnError: true)
            new Message(author: admin, target: user2, content: "a supprimer 3", isRead: true).save(flush: true, failOnError: true)
            new Message(author: admin, target: user2, content: "a supprimer 4", isRead: true).save(flush: true, failOnError: true)
            new Message(author: admin, target: user2, content: "a garder 2", isRead: false).save(flush: true, failOnError: true)
            new Message(author: admin, target: user2, content: "a garder 3", isRead: false).save(flush: true, failOnError: true)
            new Message(author: user2, target: admin, content: "test", isRead: true).save(flush: true, failOnError: true)
            new Message(author: user2, target: admin, content: "message à supprimer", isRead: true).save(flush: true, failOnError: true)




        }
    }
    def destroy = {
    }
}
