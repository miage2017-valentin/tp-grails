package fr.av.mbds.exceptions

class ApiException extends Exception {
    private int codeHTTP

    ApiException(String message, int codeHTTP) {
        super(message)
        this.codeHTTP = codeHTTP
    }

    int getCodeHTTP() {
        return codeHTTP
    }
}