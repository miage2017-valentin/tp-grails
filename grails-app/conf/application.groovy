

// Added by the Spring Security Core plugin:
grails.plugin.springsecurity.userLookup.userDomainClassName = 'fr.av.mbds.User'
grails.plugin.springsecurity.userLookup.authorityJoinClassName = 'fr.av.mbds.UserRole'
grails.plugin.springsecurity.authority.className = 'fr.av.mbds.Role'
grails.plugin.springsecurity.requestMap.className = 'fr.av.mbds.UserRole'
grails.plugin.springsecurity.securityConfigType = 'Annotation'
grails.plugin.springsecurity.successHandler.defaultTargetUrl = '/admin/index'
grails.plugin.springsecurity.logout.postOnly = false
grails.plugin.springsecurity.controllerAnnotations.staticRules = [
        [pattern: '/',               access: ['permitAll']],
        [pattern: '/error',          access: ['permitAll']],
        [pattern: '/index',          access: ['permitAll']],
        [pattern: '/index.gsp',      access: ['permitAll']],
        [pattern: '/shutdown',       access: ['permitAll']],
        [pattern: '/assets/**',      access: ['permitAll']],
        [pattern: '/**/js/**',       access: ['permitAll']],
        [pattern: '/**/css/**',      access: ['permitAll']],
        [pattern: '/**/images/**',   access: ['permitAll']],
        [pattern: '/**/favicon.ico', access: ['permitAll']],
        [pattern: '/api/login',          access: ['permitAll']],
        [pattern: '/api/logout',        access: ['isFullyAuthenticated()']],
        [pattern: '/api/message/*', access: ['isFullyAuthenticated()']],
        [pattern: '/api/messages', access: ['isFullyAuthenticated()']],
        [pattern: '/api/game/*', access: ['isFullyAuthenticated()']],
        [pattern: '/api/games', access: ['isFullyAuthenticated()']],
        [pattern: '/api/user', access: ['permitAll'], httpMethod: 'POST'],
        [pattern: '/api/user/*', access: ['isFullyAuthenticated()'], httpMethod: 'GET'],
        [pattern: '/api/user', access: ['isFullyAuthenticated()'], httpMethod: 'PUT'],
        [pattern: '/api/user', access: ['isFullyAuthenticated()'], httpMethod: 'DELETE'],
        [pattern: '/api/users', access: ['permitAll'], httpMethod: 'POST'],
        [pattern: '/api/users', access: ['isFullyAuthenticated()'], httpMethod: 'GET'],
        [pattern: '/api/users', access: ['isFullyAuthenticated()'], httpMethod: 'PUT'],
        [pattern: '/api/users', access: ['isFullyAuthenticated()'], httpMethod: 'DELETE'],
        [pattern: '/**', access: ['isFullyAuthenticated()']],
]

grails.plugin.springsecurity.filterChain.chainMap = [
		[pattern: '/assets/**',      filters: 'none'],
		[pattern: '/**/js/**',       filters: 'none'],
		[pattern: '/**/css/**',      filters: 'none'],
		[pattern: '/**/images/**',   filters: 'none'],
		[pattern: '/**/favicon.ico', filters: 'none'],
		[pattern: '/api/guest/**', filters: 'anonymousAuthenticationFilter,restTokenValidationFilter,restExceptionTranslationFilter,filterInvocationInterceptor'],
		[pattern: '/api/**', filters: 'JOINED_FILTERS,-exceptionTranslationFilter,-authenticationProcessingFilter,-securityContextPersistenceFilter,-rememberMeAuthenticationFilter'],
		[pattern: '/**',           filters: 'JOINED_FILTERS']
]


grails.plugin.springsecurity.rest.logout.endpointUrl = '/api/logout'
grails.plugin.springsecurity.rest.token.storage.jwt.secret= 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c'