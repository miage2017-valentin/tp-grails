import fr.av.mbds.User
import fr.av.mbds.UserPasswordEncoderListener
import fr.av.mbds.details.MyUserDetailsService
import grails.rest.render.json.JsonRenderer

// Place your Spring DSL code here
beans = {
    userPasswordEncoderListener(UserPasswordEncoderListener)
    userDetailsService(MyUserDetailsService)
    userJSONRenderer(JsonRenderer, User) {
        excludes = ['password']
    }
}
