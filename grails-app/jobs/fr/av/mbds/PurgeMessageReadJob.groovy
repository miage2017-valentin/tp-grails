package fr.av.mbds

class PurgeMessageReadJob {

    static triggers = {
        cron name: 'myTrigger', cronExpression: "0 0 4 * * ?"
    }
    def execute() {
        def nbMessage = Message.countByIsRead(true)
        println("Messages found : ${nbMessage}")
        println "Read message ${Message} have been deleted !"
        def list = Message.findAllByIsRead(true)
        Message.findAllByIsRead(true).each { it.delete() }
    }
}
